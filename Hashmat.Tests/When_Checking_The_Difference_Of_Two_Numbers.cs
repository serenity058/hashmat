﻿using NUnit.Framework;

namespace Hashmat.Tests
{
    [TestFixture]
    public class When_Checking_The_Difference_Of_Two_Numbers
    {
        [Test]
        public void AndTheNumbersAreEqualThenZeroIsReturned()
        {
            //Arrange
            var difference = new Hashmat();

            //Act
            var result = difference.Difference(5, 5);

            //Assert
            Assert.AreEqual(0, result);
        }
    }
}
