﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hashmat
{
    public class Hashmat

    {
        public int Difference(int a, int b)
        {
            int x;
            x = a - b;
            if(x < 0)
            {
                x *= -1;
            }
            return x;
        }
    }
}
